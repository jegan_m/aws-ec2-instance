# Creation Server

resource "aws_instance" "server" {
  count = "${var.instance_count}"
  ami = "${var.ami}"
  instance_type = "${var.ins-type}"
  subnet_id = "${var.subnet-id}"
  disable_api_termination = "${var.disable_api_termination}"
  instance_initiated_shutdown_behavior = "${var.instance_initiated_shutdown_behavior}"
  key_name = "${var.key-pair}"
  vpc_security_group_ids = ["${var.security-group}"]
  tags = merge(
    {
      "Name" = "${var.instance_count}" > 1 || "${var.use_num_suffix}" ? format("%s-%d", var.name, count.index + 1) : "${var.name}"
    },
    "${var.tags}",
  )
  root_block_device{
        volume_type="gp2"
        volume_size="10"
        delete_on_termination="true"    
    }
}
