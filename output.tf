output "instance_az" {
     value = "${aws_instance.server[0].availability_zone}"
}

output "instance_id" {
     value = "${aws_instance.server[0].id}"
}

output "instance_private_ip" {
     value = "${aws_instance.server[0].private_ip}"
}