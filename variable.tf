variable "projectname"{
default=""
}

variable "environment"{
default=""
}

/*data "aws_ami" "ubuntuos-server"{
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners=["679593333241"]
}*/

variable "ami"{
    default = ""
}

variable "ins-type"{
    default = ""
}

variable "subnet-id"{
    default=""
}

variable "instance_initiated_shutdown_behavior"{
    default = "stop"
}

variable "disable_api_termination"{
    default = ""
}


variable "security-group"{
    default = ""
}

variable "key-pair"{
default=""
}

variable "use_num_suffix" {
  description = "Always append numerical suffix to instance name, even if instance_count is 1"
  type        = bool
  default     = false
}

variable "instance_count" {
  description = "Number of instances to launch"
  type        = number
  default     = 1
}

variable "name" {
  description = "Name to be used on all resources as prefix"
  type        = "string"
  default =""
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = "map"
  default     = {}
}
